from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root@localhost:3306/viajes'
db = SQLAlchemy(app)

class Pasajero(db.Model):
    id_pasajero = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(50), nullable=False)
    apellido = db.Column(db.String(50), nullable=False)
    telefono = db.Column(db.String(20), nullable=False)

class Bus(db.Model):
    id_bus = db.Column(db.Integer, primary_key=True)
    placa = db.Column(db.String(10), nullable=False)
    modelo = db.Column(db.String(50), nullable=False)
    capacidad = db.Column(db.Integer, nullable=False)

class Viaje(db.Model):
    id_viaje = db.Column(db.Integer, primary_key=True)
    id_bus = db.Column(db.Integer, db.ForeignKey('bus.id_bus'), nullable=False)
    fecha = db.Column(db.Date, nullable=False)
    origen = db.Column(db.String(50), nullable=False)
    destino = db.Column(db.String(50), nullable=False)
    bus = db.relationship('Bus', backref=db.backref('viajes', lazy=True))

class BoletoViaje(db.Model):
    id_boleto = db.Column(db.Integer, primary_key=True)
    id_pasajero = db.Column(db.Integer, db.ForeignKey('pasajero.id_pasajero'), nullable=False)
    id_viaje = db.Column(db.Integer, db.ForeignKey('viaje.id_viaje'), nullable=False)
    fecha_compra = db.Column(db.DateTime, nullable=False)
    pasajero = db.relationship('Pasajero', backref=db.backref('boletos', lazy=True))
    viaje = db.relationship('Viaje', backref=db.backref('boletos', lazy=True))

@app.route('/viajes/<int:id_viaje>', methods=['GET'])
def get_viaje(id_viaje):
    viaje = Viaje.query.filter_by(id_viaje=id_viaje).first()
    if viaje is not None:
        pasajeros = [{'id_pasajero': b.id_pasajero, 'nombre': b.pasajero.nombre, 'apellido': b.pasajero.apellido, 'telefono': b.pasajero.telefono} for b in viaje.boletos]
        data = {
            'id_viaje': viaje.id_viaje,
            'fecha_viaje': str(viaje.fecha),
            'bus': {
                'id_bus': viaje.bus.id_bus,
                'placa': viaje.bus.placa,
                'modelo': viaje.bus.modelo,
                'capacidad': viaje.bus.capacidad
            },
            'pasajeros': pasajeros
        }
        return jsonify(data)
    else:
        return jsonify({'error': 'Viaje no encontrado'}), 404

if __name__ == '__main__':
    app.run(debug=True)