-- Crear la base de datos
CREATE DATABASE viajes;

-- Seleccionar la base de datos
USE viajes;

-- Crear la tabla pasajero
CREATE TABLE pasajero (
  id_pasajero INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nombre VARCHAR(50) NOT NULL,
  apellido VARCHAR(50) NOT NULL,
  telefono VARCHAR(20) NOT NULL
);

-- Crear la tabla bus
CREATE TABLE bus (
  id_bus INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  placa VARCHAR(10) NOT NULL,
  modelo VARCHAR(50) NOT NULL,
  capacidad INT NOT NULL
);

-- Crear la tabla viaje
CREATE TABLE viaje (
  id_viaje INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  id_bus INT NOT NULL,
  fecha DATE NOT NULL,
  origen VARCHAR(50) NOT NULL,
  destino VARCHAR(50) NOT NULL,
  FOREIGN KEY (id_bus) REFERENCES bus(id_bus)
);

-- Crear la tabla boleto_viaje
CREATE TABLE boleto_viaje (
  id_boleto INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  id_pasajero INT NOT NULL,
  id_viaje INT NOT NULL,
  fecha_compra DATETIME NOT NULL,
  FOREIGN KEY (id_pasajero) REFERENCES pasajero(id_pasajero),
  FOREIGN KEY (id_viaje) REFERENCES viaje(id_viaje)
);


-- Insertar datos de ejemplo en la tabla pasajero
INSERT INTO pasajero (nombre, apellido, telefono) VALUES 
('Juan', 'García', '555-1234'),
('María', 'López', '555-5678'),
('Pedro', 'González', '555-9012'),
('Ana', 'Martínez', '555-3456'),
('José', 'Hernández', '555-7890'),
('Sofía', 'Díaz', '555-2345'),
('Luis', 'Torres', '555-6789'),
('Carmen', 'Rivera', '555-0123'),
('Antonio', 'Reyes', '555-4567'),
('Laura', 'Ramírez', '555-8901'),
('Diego', 'Alvarez', '555-2341'),
('Gloria', 'Fernández', '555-6783'),
('Ricardo', 'Castro', '555-0124'),
('Fernanda', 'Ortiz', '555-4569'),
('Miguel', 'Sánchez', '555-8903');


-- Insertar datos de ejemplo en la tabla bus
INSERT INTO bus (placa, modelo, capacidad) VALUES 
('ABC-123', 'Volvo', 30),
('DEF-456', 'Mercedes-Benz', 25),
('GHI-789', 'MAN', 35);

-- Insertar datos de ejemplo en la tabla viaje
INSERT INTO viaje (id_bus, fecha, origen, destino) VALUES 
(1, '2023-05-12', 'Ciudad de México', 'Guadalajara'),
(1, '2023-05-13', 'Guadalajara', 'Ciudad de México'),
(1, '2023-05-15', 'Ciudad de México', 'Puebla'),
(2, '2023-05-16', 'Puebla', 'Ciudad de México'),
(2, '2023-05-18', 'Ciudad de México', 'Monterrey'),
(2, '2023-05-19', 'Monterrey', 'Ciudad de México'),
(2, '2023-05-20', 'Ciudad de México', 'Tijuana'),
(3, '2023-05-21', 'Tijuana', 'Ciudad de México'),
(3, '2023-05-22', 'Ciudad de México', 'Mérida'),
(3, '2023-05-24', 'Mérida', 'Ciudad de México');

-- Insertar datos de ejemplo en la tabla boleto_viaje
INSERT INTO boleto_viaje (id_pasajero, id_viaje, fecha_compra) VALUES 
(1, 1, '2023-05-11 08:00:00'),
(2, 1, '2023-05-11 09:00:00'),
(3, 1, '2023-05-11 10:00:00'),
(4, 2, '2023-05-11 11:00:00'),
(5, 2, '2023-05-11 12:00:00'),
(6, 3, '2023-05-11 13:00:00'),
(7, 3, '2023-05-11 14:00:00'),
(8, 4, '2023-05-11 15:00:00'),
(9, 4, '2023-05-11 16:00:00'),
(10, 5, '2023-05-11 17:00:00'),
(11, 6, '2023-05-11 18:00:00'),
(12, 6, '2023-05-11 19:00:00'),
(13, 7, '2023-05-11 20:00:00'),
(14, 7, '2023-05-11 21:00:00'),
(15, 8, '2023-05-11 22:00:00'),
(14, 9, '2023-05-11 23:00:00'),
(13, 9, '2023-05-12 00:00:00'),
(12, 10, '2023-05-12 01:00:00'),
(11, 10, '2023-05-12 02:00:00'),
(10, 10, '2023-05-12 03:00:00'),
(1, 1, '2023-05-12 08:00:00'),
(2, 1, '2023-05-13 09:00:00'),
(3, 2, '2023-05-13 10:00:00'),
(4, 3, '2023-05-15 11:00:00'),
(5, 4, '2023-05-16 12:00:00'),
(6, 5, '2023-05-18 13:00:00'),
(7, 6, '2023-05-19 14:00:00'),
(8, 7, '2023-05-20 15:00:00'),
(9, 8, '2023-05-21 16:00:00'),
(8, 9, '2023-05-22 17:00:00');


-- select
SELECT b.id_bus, p.nombre, p.apellido, v.fecha
FROM viaje v
JOIN bus b ON b.id_bus = v.id_bus
JOIN boleto_viaje bv ON bv.id_viaje = v.id_viaje
JOIN pasajero p ON p.id_pasajero = bv.id_pasajero
WHERE v.id_viaje = 5;

